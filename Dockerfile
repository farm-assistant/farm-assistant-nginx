FROM openresty/openresty:trusty

WORKDIR /opt

COPY . $PWD

RUN apt-get update
RUN apt-get install libssl-dev -y
RUN /usr/local/openresty/luajit/bin/luarocks install bcrypt

ENTRYPOINT ["./run"]

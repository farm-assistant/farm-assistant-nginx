#!/bin/sh -x
 
# Script to compile nginx on ubuntu with lua support.
 
NGX_VERSION='1.10.0'
LUAJIT_VERSION='2.0.4'
LUAJIT_MAJOR_VERSION='2.0'
NGX_DEVEL_KIT_VERSION='0.2.19'
LUA_NGINX_MODULE_VERSION='0.10.2'
LUA_ROCKS_VERSION=2.2.1

home=`pwd`

NGINX_INSTALL_PATH='/usr/local/nginx'
 
# Download
if [ ! -f ./nginx-${NGX_VERSION}.tar.gz ]; then
    wget http://nginx.org/download/nginx-${NGX_VERSION}.tar.gz
fi
 
if [ ! -f ./LuaJIT-${LUAJIT_VERSION}.tar.gz ]; then
    wget http://luajit.org/download/LuaJIT-${LUAJIT_VERSION}.tar.gz
fi
 
if [ ! -d ./ngx_devel_kit ]; then
    #wget https://github.com/simpl/ngx_devel_kit/archive/v${NGX_DEVEL_KIT_VERSION}.tar.gz \
       # -O ngx_devel_kit-${NGX_DEVEL_KIT_VERSION}.tar.gz
    git clone https://github.com/simpl/ngx_devel_kit;
fi
 
if [ ! -f ./lua-nginx-module-${LUA_NGINX_MODULE_VERSION}.tar.gz ]; then
    wget https://github.com/openresty/lua-nginx-module/archive/v${LUA_NGINX_MODULE_VERSION}.tar.gz \
        -O lua-nginx-module-${LUA_NGINX_MODULE_VERSION}.tar.gz
fi
 
# Lua Rocks for bcrypt
if [ ! -d  ./luarocks-${LUA_ROCKS_VERSION} ]; then
   wget http://luarocks.github.io/luarocks/releases/luarocks-${LUA_ROCKS_VERSION}.tar.gz
fi
 
# Extract
if [ ! -d ./nginx-${NGX_VERSION} ]; then
    tar xvf nginx-${NGX_VERSION}.tar.gz
fi
 
if [ ! -d ./LuaJIT-${LUAJIT_VERSION} ]; then
    tar xvf LuaJIT-${LUAJIT_VERSION}.tar.gz
fi
 
if [ ! -d ./lua-nginx-module-${LUA_NGINX_MODULE_VERSION} ]; then
    tar xvf lua-nginx-module-${LUA_NGINX_MODULE_VERSION}.tar.gz
fi

if [ ! -d  ./luarocks-${LUA_ROCKS_VERSION}.tar.gz ]; then
	tar xvf luarocks-${LUA_ROCKS_VERSION}.tar.gz
fi

# Install PCRE devel tools and libssl
apt-get -y install --no-install-recommends libghc-regex-pcre-dev libssl-dev unzip
 
 
# Install luajit
cd ./LuaJIT-${LUAJIT_VERSION} && sudo make install && cd ..
 
NGX_DEVEL_KIT_PATH=$(pwd)/ngx_devel_kit
LUA_NGINX_MODULE_PATH=$(pwd)/lua-nginx-module-${LUA_NGINX_MODULE_VERSION}
 
# Compile And Install Nginx
cd ./nginx-${NGX_VERSION} && \
    ./configure --prefix=${NGINX_INSTALL_PATH} --conf-path=${NGINX_INSTALL_PATH}/nginx.conf --pid-path=/var/run/nginx.pid \
    --sbin-path=/usr/local/sbin/nginx --lock-path=/var/run/nginx.lock \
    --with-ld-opt='-Wl,-rpath,/usr/local/lib/lua' \
    --with-http_stub_status_module  \
    --add-module=${NGX_DEVEL_KIT_PATH} \
    --add-module=${LUA_NGINX_MODULE_PATH} \
    && make -j4 && sudo make install

# Install luarocks and bcrypt module

   cd ${home}
   cd ./luarocks-${LUA_ROCKS_VERSION}
   ./configure --with-lua-include=${home}/LuaJIT-${LUAJIT_VERSION}/src
   make bootstrap

   cd ${home}
   export CPATH=${home}/LuaJIT-${LUAJIT_VERSION}/src
   sudo luarocks install luasocket
   sudo luarocks install bcrypt

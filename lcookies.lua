local _C = {}

function get_cookies()
  local cookies = ngx.header["Set-Cookie"] or {}
 
  if type(cookies) == "string" then
    cookies = {cookies}
  end
 
  return cookies
end
 
function _C.add_cookie(cookie,expiry)
  if (expiry == nil) then
        expiry = 0
  end
  local cookies = get_cookies()
  table.insert(cookies, cookie)
  ngx.header['Set-Cookie'] = cookies
  ngx.cookie_time(ngx.time() + expiry)
end


return _C
